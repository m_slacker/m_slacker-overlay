# Copyright 1999-2009 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=4

inherit distutils

DESCRIPTION="A modular machine learning library for Python"
HOMEPAGE="http://www.pybrain.org/"
SRC_URI="https://github.com/pybrain/pybrain/tarball/${PV} -> ${P}.tar.gz"
LICENSE="BSD"
SLOT="0"
KEYWORDS="~amd64"
IUSE=""
RDEPEND=""
DEPEND="${RDEPEND}"
S=${WORKDIR}/pybrain-pybrain-87c7ac3

src_install() {
	distutils_src_install
	cp -r docs "${D}"usr/share/doc/${PF}/ || die
	dodir /usr/share/${PN}/
	cp -r examples "${D}"usr/share/${PN}/ || die
}
