# Copyright 1999-2014 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=4

DESCRIPTION="Independent set of plugins for Apache JMeter"
HOMEPAGE="http://jmeter-plugins.org/"
SRC_URI="http://jmeter-plugins.org/downloads/file/JMeterPlugins-Standard-${PV}.zip"

LICENSE="Apache-2.0"
SLOT="0"
KEYWORDS="~amd64"
IUSE=""

DEPEND="=app-benchmarks/jmeter-bin-2.11"
RDEPEND="${DEPEND}"

S="${WORKDIR}"

src_prepare() {
	rm {LICENSE,README}
}

src_install() {
	insinto "/opt/jmeter-bin/apache-jmeter-2.11"
	doins -r *
}
