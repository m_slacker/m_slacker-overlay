# Copyright 1999-2014 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=4

DESCRIPTION="Manager of plugins for Apache JMeter"
HOMEPAGE="http://jmeter-plugins.org/"

LICENSE="Apache-2.0"
SLOT="0"
KEYWORDS="~amd64"
IUSE=""

DEPEND="=app-benchmarks/jmeter-bin-3.3"
RDEPEND="${DEPEND}"

S="${WORKDIR}"

src_unpack() {
	wget "https://repo1.maven.org/maven2/kg/apc/jmeter-plugins-manager/${PV}/jmeter-plugins-manager-${PV}.jar"
}

src_install() {
	insinto "/opt/jmeter-bin/apache-jmeter-3.3/lib/ext"
	doins -r *
}
