# Copyright 1999-2010 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: /var/cvsroot/gentoo-x86/app-benchmarks/jmeter/jmeter-2.0.1-r4.ebuild,v 1.2 2008/10/24 20:36:23 maekke Exp $

DESCRIPTION="Load test and measure performance on HTTP/FTP services and databases."
HOMEPAGE="http://jakarta.apache.org/jmeter"
#SRC_URI="http://apache-mirror.rbc.ru/pub/apache/jmeter/binaries/apache-jmeter-${PV}.tgz"
SRC_URI="https://archive.apache.org/dist/jmeter/binaries/apache-jmeter-${PV}.tgz"
LICENSE="Apache-2.0"

SLOT="0"
IUSE="doc"
KEYWORDS="~amd64 ~x86"

DEPEND=">=virtual/jdk-1.6"
RDEPEND="${DEPEND}"

# TODO: remove "apache-jmeter-2.11" from path
S="${WORKDIR}/jakarta-jmeter-${PV}/"

src_prepare() {
	cd "${S}/bin/"
	rm -f *.bat *.cmd
}

src_install() {
	dodir /opt/${PN}
	cp -aR * "${D}/opt/${PN}/"
	use doc || rm -fR "${D}/opt/${PN}/docs"
}


